
let section = document.querySelector('.section');
// console.log(section);

let cardsData=[
    {imgSrc :'https://pics.clipartpng.com/thumbs/Green_Apple_PNG_Clip_Art_Image-2714.png', imgName:'Green-apple'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Cherries_Fruit_PNG_Clipart-216.png',imgName:'cherry'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Kiwi_Fruit_PNG_Clipart-226.png',imgName:'kiwi'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Watermelon_PNG_Clip_Art-2912.png',imgName:'waterMelon'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Strawberry_PNG_Clipart-261.png',imgName:'stawbery'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Orange_with_Leaf_PNG_Clipart-237.png',imgName:'orange'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/White_Grapes_PNG_Clipart-264.png',imgName:'grapes'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Bananas_PNG_Clipart-209.png',imgName:'banana'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Green_Apple_PNG_Clip_Art_Image-2714.png', imgName:'Green-apple'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Cherries_Fruit_PNG_Clipart-216.png',imgName:'cherry'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Kiwi_Fruit_PNG_Clipart-226.png',imgName:'kiwi'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Watermelon_PNG_Clip_Art-2912.png',imgName:'waterMelon'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Strawberry_PNG_Clipart-261.png',imgName:'stawbery'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Orange_with_Leaf_PNG_Clipart-237.png',imgName:'orange'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/White_Grapes_PNG_Clipart-264.png',imgName:'grapes'},
    {imgSrc :'https://pics.clipartpng.com/thumbs/Bananas_PNG_Clipart-209.png',imgName:'banana'}
];

//make a new card and add the front and back and add the image in front side
//use the sort to get a card randomely
cardsData.sort(()=>{
    return 0.5 - Math.random();
}).forEach((currCard)=>{
    let card = document.createElement('div');
    let back = document.createElement('div');
    let front = document.createElement('div');
    let image = document.createElement('img');
    front.classList = 'front';
    card.classList = 'card';
    back.classList = 'back';
    image.setAttribute('src', currCard.imgSrc);
    image.setAttribute('alt', currCard.imgName);
    card.id = currCard.imgName;
    front.appendChild(image);
    card.appendChild(front);
    card.appendChild(back);
    section.appendChild(card);
    return card.innerHTML;
});

//check a cards previous and current card 
const matchedObj = {};
function checkCard(currCard){
        if(storedCard.id==currCard.id){
            matchedObj[storedCard.id] = 1;
            console.log('matched' + Object.keys(matchedObj).length);
            console.log(matchedObj);
            storedCard=undefined;
            twoCards=0
        }else{
            setTimeout(() => {
                currCard.classList.toggle('card-toggle');
                storedCard.classList.toggle('card-toggle');
                console.log("inside set",storedCard)
                storedCard=undefined;
                twoCards=0
            }, 1000);
        }
}

//counter is used to count the seconds and it update the time
let counter = 0;
let timerInterval;
function startTimer(){
    clearInterval(timerInterval);
    timerInterval = setInterval(updateTimer, 1000);
}
function updateTimer() {
    counter++;
    document.querySelector('#times').innerText = `${counter} sec`;
    if(counter >= 500 || Object.keys(matchedObj).length >= 8){        
        clearInterval(timerInterval);
        makeItNormal(); 
        document.querySelector('#times').innerText = 'stopped !!!';
    }
}


//add event listener to button to change the innertext of button
let buttonClick = false;
document.querySelector('button').addEventListener('click',(event)=>{
    if(buttonClick == false){
        buttonClick = true;
        document.querySelector('button').firstChild.innerText = 'Start !';
        twoCards = 10;
        console.log( event.target, 'stopped');
        clearInterval(timerInterval);
    }
    else{
        twoCards = 0;
        buttonClick = false;
        document.querySelector('button').firstChild.innerText = 'Started';
        console.log( event.target, 'started');
        startTimer();
    }
});


let twoCards =0;
let storedCard=undefined; 
let clickCount = 0; //we need to count click for counting the moves

section.addEventListener('click',(event)=>{
    console.log(event.target.parentElement);
    let card = event.target.parentElement;
    console.log(twoCards);
    if(event.target.classList.contains('back')  && twoCards <2){
        //start the timer first 
        startTimer();
        //change the text of button and there color
        document.querySelector('button').firstChild.innerText = 'Pause';
        document.querySelector('button').style.color = 'white';
        
        ++clickCount;
        document.querySelector('#moves').innerText = clickCount;

        //add toggle to the clicked card so it will show us the front side image
        card.classList.toggle('card-toggle');
        twoCards+=1;
        if (storedCard == undefined) {
            storedCard = card;
        }
        else{
        console.log( event.target);
        checkCard(card);
        } 
    }
});

function makeItNormal(){
    document.querySelector('h3').innerText = `You Won The Game in ${counter} seconds and it takes ${clickCount} Moves Only !`;
    document.querySelector('button').firstChild.innerText = 'Restart';
    document.querySelector('button').addEventListener('click', (event)=>{
      if(event.target){
            location.reload();
      }
    });
}